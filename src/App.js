import React,{ Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import { Container, Row, Col } from 'reactstrap';
import mascot from './images/mascot.png';
import './App.css';

class App extends Component{
	render(){
	  return (
		<div className="App">
		  
		  <React.Fragment>
			<Header/>
			<Index/>
		  </React.Fragment>
		  
		</div>
	  );
	}
}
export default App;

class Header extends Component{
	render(){
		return(
			<header className="App-header">
				<h1>
				  NowaJoestar's Cave
				</h1>
				<Navbar/>
			</header>
		);
	}

}

class Navbar extends React.Component{
    render() {
        return (
            <div>
              <ul id="nav">
				<li><a href="#">About</a></li>
                <li><a href="#">Home</a></li>
                <li><a href="#">FAQ</a></li>
                <li><a href="#">Contact</a></li>
              </ul>
            </div>
        );
    }
}

class Index extends Component{
	render(){
		return(
			<Container className="App-index" fluid>	
				<Row>
					<Col xs = "2"></Col>
					<Col xs = "8">
						<h1>[About this site]</h1>
						<p>This site is designed to share my basic information and anunce my current proyects.</p>
						<p>WARNING: The contend of this site is R18</p>
						<h1>[NowaJoestar]</h1>
						<p>Hey, I'm "NowaJoestar", a NSFW Peruvian Artist.</p>
						<p>"Y recuerda chavito, la censura nunca es buena mata el alma y la envenena".</p>
					</Col>
					<Col xs = "2"></Col>
				</Row>
			
				<Row>
				
					<Col xs = "6">		
						<Row>
							<Col xs = "12">
								<img src={require('./images/icons/Twitter.png')} height="30px"/>
								<a
								  className="App-link"
								  href="https://twitter.com/NowaPLS"
								  target="_blank"
								  rel="noopener noreferrer"
								>
								  TWITTER
								</a>
							</Col>
						</Row>
						<Row>
							<Col xs = "12">
								<img src={require('./images/icons/Facebook.png')} height="30px" />
								<a
								  className="App-link"
								  href="https://www.facebook.com/AntroNowaJoestar"
								  target="_blank"
								  rel="noopener noreferrer"
								>
								  FACEBOOK PAGE
								</a>
							</Col>
						</Row>
						<Row>
							<Col xs = "12">
								<img src={require('./images/icons/Facebook.png')} height="30px" />
								<a
								  className="App-link"
								  href="https://www.facebook.com/groups/709708056192499"
								  target="_blank"
								  rel="noopener noreferrer"
								>
								  FACEBOOK GROUP
								</a>
							</Col>
						</Row>
						<Row>
							<Col xs = "12">
								<img src={require('./images/icons/Newgrounds.png')} height="30px" />
								<a
								  className="App-link"
								  href="https://nowajoestar.newgrounds.com"
								  target="_blank"
								  rel="noopener noreferrer"
								>
								  NEWGROUNDS
								</a>
							</Col>
						</Row>
						<Row>
							<Col xs = "12">
								<img src={require('./images/icons/Pixiv.png')} height="30px" />
								<a
								  className="App-link"
								  href="https://www.pixiv.net/member.php?id=5956575"
								  target="_blank"
								  rel="noopener noreferrer"
								>
								  PIXIV
								</a>
							</Col>
						</Row>
						<Row>
							<Col xs = "12">
								<img src={require('./images/icons/HF.png')} height="30px" />
								<a
								  className="App-link"
								  href="https://www.hentai-foundry.com/user/NowaJoestar/profile"
								  target="_blank"
								  rel="noopener noreferrer"
								>
								  HENTAI FOUNDRY
								</a>
							</Col>
						</Row>
						<Row>
							<Col xs = "12">
								<img src={require('./images/icons/Patreon.png')} height="30px" />
								<a
								  className="App-link"
								  href="https://www.patreon.com/join/NowaJoestar"
								  target="_blank"
								  rel="noopener noreferrer"
								>
								  PATREON
								</a>
							</Col>
						</Row>
						<Row>
							<Col xs = "12">
								<img src={require('./images/icons/Paypal.png')} height="30px" />
								<a
								  className="App-link"
								  href="https://paypal.me/NowaJoestar?locale.x=es_XC"
								  target="_blank"
								  rel="noopener noreferrer"
								>
								  PAYPAL
								</a>
							</Col>
						</Row>
					</Col>
					
					<Col xs = "6">
						<img src={mascot} className="App-mascot" alt="mascot" />
					</Col>
					
				</Row>
				
			</Container>
			

		);
	}
}